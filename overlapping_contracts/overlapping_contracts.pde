Contract[] contracts;

void setup() {
  loadData();
}

void draw() {
  
}

void loadData () {
  String lines[] = loadStrings("data/contract_dates.csv");
  for (int i = 0 ; i < lines.length; i++) {
    println(lines[i]);
  }
}
